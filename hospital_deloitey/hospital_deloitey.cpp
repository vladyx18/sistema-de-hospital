// PROGRAMA PARA HOSPITAL DELOITEY
// POR EL GRUPO DE ALGORITMOS DE LA CARRERA DE ING. EN SISTEMAS 2DO. SEMESTRE
// VLADIMIR MUNRAYOS, JUAN LUIS SANDOVAL, LUIS ANDRES CATALAN, RICKY AVALONY, JEISSON GALLARDO, LESTER NAJERA
// 1190-14-1874		  1190-14-1808        1190-14-11040        1190-14-3967   1190-14-2158      1190-10-02205

// Recordatorio: Los archivos de texto se encuentran en la carpeta llamada: hospital_deloitey

#include "stdafx.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <conio.h>
#include <iostream>
#include <fstream>
#include <string>

// variables que utilizamos
char p1[30];
char p2[30];
char p3[30];
char d[30];
char o[30];
int ca;
float pu;
int correcto;
int correcto2;
int intento2;
int intento;
char admin[6];
char pass[5];
const char codigo[]="1023";
const char usuario[]="admin";

using namespace std;

int main(int argc, char *argv[])
{
   // inicio del programa, en el cual llamamos las variables que nos ayudaran a que funcione la parte de la autentificacion del usuario correctamente.
   intento=1;
   intento2=1;
   correcto2=0;
   correcto=0;
   system ("color 04");
   printf_s("**** BIENVENIDO AL SISTEMA DE ADMINISTRACION DEL HOSPITAL DELOITEY ****\n");
   cout<<"Ingrese nombre de usuario\t"; gets_s(admin);
   if(strcmp(admin,usuario)==0) correcto=1;
   while((correcto==0)&&(intento<1))
   {
        intento++;
        printf("", intento); gets_s(admin); printf("\n");
        if (strcmp(admin,usuario)==0) correcto=1;
    }
   if (correcto==0) {
		printf("Su nombre de usuario es incorrecto, -->");
		system("pause");
		return 0;
	}
   else{
   cout<<"Ingrese codigo\t"; gets_s(pass);
   if(strcmp(pass,codigo)==0) correcto2=1;
   while((correcto2==0)&&(intento2<1))
   {
	   intento2++;
	   printf("", intento2); gets_s(pass); printf("/n");
	   if (strcmp(pass,codigo)==0) correcto2=1;
   }
    if (correcto2==0) {
		printf("Su codigo es incorrecto, -->");
		system("pause");
		return 0;
	}

	  // si anteriormente se cumple el usuario y la contraseņa correctamente entrara satisfactoriamente al menu. 
      else{
	   int opr;
	   menu:
	   cout<<"1:Visualizacion del inventario\n";
	   cout<<"2:Almacenar pacientes\n";
	   cout<<"3:Realizar calculos del productos\n";
	   cout<<"4:Salir\n";
	   scanf_s("%d",&opr);
	   
	   // si se escoge la primera opcion mostrara el inventario mediante el archivo de txt. que se creo llamado inventario. 
	   if(opr==1){
	    string readfromfile;
		ifstream r_file("inventario.txt");

		if(r_file.is_open())
		{
			while(r_file.good()){
				getline(r_file, readfromfile);
				cout<<readfromfile <<endl <<endl;
			}

		}else
			cout<<"El archivo no existe!" <<endl;

		system ("pause");
		goto menu;  // ayuda a regresar al menu.

	   }

	   // si escoge la segunda opcion, podra almacenar los nombres de los pacientes en un archivo de txt. llamado almacenamiento.
	   if(opr==2){
       cout<<"Ingrese 1er. paciente\t";
       cin>>p1;
       cout<<"Ingrese 2do. paciente\t";
       cin>>p2;
       cout<<"Ingrese 3er. paciente\t";
       cin>>p3;   
       cout<<"Se ha escrito bien el archivo -->\t";
       ofstream writer("almacenamiento.txt");
       if(writer.is_open())
       {
         int paciente = 3;
         writer << " Nombre del 1er. paciente\t";writer<<p1<<endl;
         writer << " Nombre del 2do. paciente\t"; writer<<p2<<endl;
         writer << " Nombre del 3er. paciente\t"; writer<<p3<<endl;
         writer << " El no. de pacientes es =\t" << paciente << " pacientes"<<endl;
         system("pause");
         goto menu;  // ayuda a regresar al menu.
     }
   }

	   // si se escoge la tercera opcion, el programa dara visualizacion de un menu estilo el inventario solo que esta ves funciona como un cajero que a la hora de que le compren una medicina
	   // pida la cantidad, la descripcion, el precio unitario, y el total que se da por medio de la multiplicacion entre la cantidad y el precio unitario. Luego el usuario dira si desea dar
	   // alguna observacion del medicamento.
	   if(opr==3){
		   cout<<"Ingrese cantidad\t";
		   cin>> ca;
		   cout<<"Ingrese descripcion\t";
		   cin.ignore();
		   cin.getline(d,30);
		   cout<<"Ingrese Precio Unitario\t";
		   cin>> pu;
		   cout<<"Su total es =\t" <<ca*pu<<"\n";
		   cout<<"Ingrese Observaciones\t";
		   cin.ignore();
		   cin.getline(o,30);
		   system("pause");
		   goto menu; // ayuda a regresar al menu.
		   return 0;

	   }
	  }

   }
}